require 'rails_helper'
# require "savon"
require "savon/mock/spec_helper"
require 'rexml/document'
require 'rexml/formatters/pretty'
require 'byebug'

RSpec.describe YodanslationsController, type: :request do
  include Savon::SpecHelper
  let(:text) { 'You will find many planets to conquer' }
  # let(:response) { YodaTalkClient.translate(text) }
  let(:response) {
    stub_request(:get, "http://www.yodaspeak.co.uk/webservice/yodatalk.php?wsdl").
      with(
        headers: {
          'Accept'=>'*/*',
          'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'User-Agent'=>'Ruby'
          }).
      to_return(status: 200, body: "", headers: {})
  }
  let(:result) do
    {
      status: 200,
      body: file_fixture('yoda_talk.xml').read,
      headers: {
        'Accept'=>'*/*',
        'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent'=>'Ruby'
      },
    }
  end

  before(:all) { savon.mock! }
  after(:all)  { savon.unmock! }

  it "mocks a SOAP request" do
    # fixture = File.read("spec/fixtures/yoda_talk.xml")
    # fixture = REXML::Document.new(response)
    # savon.expects(:yoda_talk).with(message: { yoda_talk: text }).returns(fixture)

    expect(result[:status]).to be(200)
  end
end