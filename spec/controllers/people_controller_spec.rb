require 'rails_helper'

RSpec.describe PeopleController, type: :request do
  let(:request_url) { '/people/create' }
  let(:request_headers) { { CONTENT_TYPE: 'application/json', ACCEPT: 'application/json' } }
  subject do
    get request_url, headers: request_headers
    response
  end

  let(:stubbed_response) do
    {
      "count": 2,
      "results": [
        {
          "name": "C-3PO",
          "height": "167",
          "gender": "n/a",
        },
        {
          "name": "Obi-Wan Kenobi",
          "height": "182",
          "gender": "male",
        }
      ]
    }
  end

  let!(:stub) do
    stub_request(:get, "https://swapi.co/api/people").
      to_return({status: 200, body: stubbed_response.to_json})
  end

  describe "GET #create" do
    it "returns http success" do
      is_expected.to have_http_status(:success)
    end

    context "responds with expected body" do
      before { SwapiClient.class_variable_set(:@@people, nil) }
      let(:stubbed_response) do
        {
          "count": 1,
          "results": [
            {
              "name": "Beru Whitesun lars",
              "height": "165",
              "gender": "female"
            }
          ]
        }
      end
      let(:expected_body) do
        {
          'name' => "Beru Whitesun lars",
          'height' => 165,
          'gender' => "female"
        }
      end

      it { expect(subject.parsed_body).to eq(expected_body) }
    end

    context ''
  end

end
