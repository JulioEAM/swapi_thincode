Rails.application.routes.draw do
  get 'people/create', format: "json"
  get 'planets/create', format: "json"
  post 'yodanslations/capture', format: "json"
  get 'yodanslations/capture'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
