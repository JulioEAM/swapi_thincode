require 'rest-client'
require 'json'

class SwapiClient
  SWAPI_URL = 'https://swapi.co/api'

  def self.get(resource, options={})
    url = "#{SWAPI_URL}/#{resource}"
    response = RestClient.get(url)
    JSON.parse(response)
  end

  def self.planets
    @@planets ||= get("planets")
  end

  def self.planet
    planets['results'].sample
  end

  def self.planet_count
    planets['count']
  end

  def self.people
    @@people ||= get("people")
  end

  def self.person
    people['results'].sample
  end

  def self.person_count
    people['count']
  end

end