require 'savon'
require 'byebug'

class YodaTalkClient
  YODA_CLIENT = Savon.client(
    wsdl: 'http://www.yodaspeak.co.uk/webservice/yodatalk.php?wsdl',
  )

  def self.translate(text)
    byebug
    response = YODA_CLIENT.call(:yoda_talk, message: {
      yoda_talk: text
    } ).to_hash[:yoda_talk_response]
  end
end
