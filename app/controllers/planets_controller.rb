class PlanetsController < ApplicationController

  def create
    begin
      planet = SwapiClient.planet
      @planet = Planet.create!(
        name: planet['name'],
        climate: planet['climate'],
        population: planet['population']
      )
    rescue ActiveRecord::RecordInvalid
      retry if Planet.count < SwapiClient.planet_count
      raise
    end
  end
end
