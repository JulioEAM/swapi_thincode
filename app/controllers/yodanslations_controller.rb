class YodanslationsController < ApplicationController
  def translate
    @translated = YodaTalkClient.translate(params[:sentence])
  end

  def capture
    translate if params[:sentence]
  end
end