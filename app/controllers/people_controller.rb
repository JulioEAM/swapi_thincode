class PeopleController < ApplicationController

  def create
    begin
      person = SwapiClient.person
      @person = Person.create!(
        name: person['name'],
        height: person['height'],
        gender: person['gender']
      )
    rescue ActiveRecord::RecordInvalid
      retry if Person.count < SwapiClient.person_count
      raise
    end
  end
end
