class ChangePopulationToBigint < ActiveRecord::Migration[6.0]
  def change
    change_column "planets", "population", :bigint
  end
end
